#ifndef Packet_Reciver_HEADER
#define Packet_Reciver_HEADER

#include "Intercom_datenpacket.h"
#include "Commands_Datenpacket.h"
#include "Output_Datenpacket.h"
#include "Sensor_Datenpacket.h"
#include <vector>
class Packet_Reciever
{


public:
	Packet_Reciever();

	Intercom_datenpacket reciveIntercom(std::vector<unsigned char>  packet);

	Sensor_Datenpacket reciveSensor(std::vector<unsigned char>packet);

	Output_Datenpacket reciveOutput(std::vector<unsigned char> packet);

	Commands_Datenpacket reciveComand(std::vector<unsigned char> packet);

};



#endif
