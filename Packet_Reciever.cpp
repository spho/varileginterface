#include <iostream>
#include "Packet_Reciever.h"


Packet_Reciever::Packet_Reciever()
{


}

Intercom_datenpacket Packet_Reciever::reciveIntercom(std::vector<unsigned char>  packet)
{
	Intercom_datenpacket dp;
	int check = packet[0];
	if (check != 4){
		//throw new System.ArgumentException("Datapacket is not an Intercompacket", "packet");
		std::cout << "Exception in Packetreciver at recive Intercom. ID is: "<<packet[0]<<"\n";
		return dp;
	}
	else{
		dp.convertFromByte(packet);
	
		return dp;
	}

}


Sensor_Datenpacket Packet_Reciever::reciveSensor(std::vector<unsigned char> packet)
{
	Sensor_Datenpacket dp;
	int check = packet[0];
	if (check != 1)
	{
		//throw new System.ArgumentException("Datapacket is not an Sensorpacket", "packet");
		std::cout << "Exception in Packetreciver at recive Sensor";
		return dp;
	}
	else{
		dp.convertFromByte(packet);
		return dp;
	}
}


Output_Datenpacket Packet_Reciever::reciveOutput(std::vector<unsigned char>  packet)
{
	Output_Datenpacket dp;
	int check = packet[0];
	if (check != 2)
	{
		//throw new System.ArgumentException("Datapacket is not an Outputpacket", "packet");
		std::cout << "Exception in Packetreciver at recive Output";
		return dp;
	}
	else{
		dp.convertFromByte(packet);
		return dp;
	}
}



Commands_Datenpacket Packet_Reciever::reciveComand(std::vector<unsigned char>  packet)
{
	Commands_Datenpacket dp;
	int check = packet[0];
	if (check != 3)
	{
		//throw new System.ArgumentException("Datapacket is not an Commandspacket", "packet");
		std::cout << "Exception in Packetreciver at recive Comand";
		return dp;
	}
	else{
		dp.convertFromByte(packet);
		return dp;
	}
}

