ADD_FILES =  IDatenPacket.cpp Intercom_datenpacket.cpp  Packet_Reciever.cpp Sensor_Datenpacket.cpp Output_Datenpacket.cpp Commands_Datenpacket.cpp
ADD_LIBRARIES = -lzmq

all: BA



BA: BA_Framework.cpp
	g++ -Wall  --std=c++11 -O3 -o BA ${ADD_FILES} BA_Framework.cpp ${ADD_LIBRARIES}

clean: 
	rm -f  BA
