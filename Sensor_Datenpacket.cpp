#include "Sensor_Datenpacket.h"
#include <vector>
#include <cstring>
#include <iostream>


using namespace std;

 void Sensor_Datenpacket::setTime(long ti){
	 this->time = ti;
}

unsigned long Sensor_Datenpacket:: get_time()
{
	return time;
}

 Sensor_Datenpacket::Sensor_Datenpacket()
{
	
	 //Anzahl Floats welche zu einem Bytearray gepackt werden und wieder zur�ck converteriet werden
	this->numbersOfFloats = 100;

	 //Erste Nummer ist die Auswahl der IMU
	 //0 Fuss rechts, 1 Unterschenkel rechts, 2 Oberschenkel rechts, 3 Fuss links, 4 Unterschenkel links, 5 Oberschenkel links, 6 R�cken
	 //Zweite Auswhal ist die Auswahl des Werts
	 //0 Gyro X, 1 Gyro Y, 2 Gyro Z, 3 Acc X, 4 Acc Y, 5 Acc Z
	//this->IMU = new float[7][ 6];

	this->IMU.resize(7);
	for(int i =0;i<7;++i)
	{
		this->IMU[i].resize(6);
	}

	 //Erste Nummer ist auswahl rechts (0) links (1)
	 //Zweite Nummer ist auswahl der Postition
//	this->LOADCELL = new float[2][ 6];
	this->LOADCELL.resize(2);
	for(int i =0;i<2;++i)
	{
		this->LOADCELL[i].resize(6);
	}
	 //Erste Nummer ist auswahl motor
	 //0 Motor knie rechts, 1 Motor rolle rechts, 2 Motor h�fte rechts, 3 motor knie links, 4 motor rolle links, 5 motor h�fte links
	 //Zweite Nummer ist auswahl funktion
	 //0 RPM, 1 Temperatur, 2 Motorenstrom
	//this->MOTORENWERTE = new float[6][ 3];
	this->MOTORENWERTE.resize(6);
	for(int i =0;i<6;++i)
	{
		this->MOTORENWERTE[i].resize(3);
	}

	 //0 Hotpot knie link, 1 hotpot hebel links, 2 hotpot rolle link, 3 hotpot knie rechts, 4 hotpot hebel rechts, 5 hotpot rolle rechts, 6 Poti h�fte links, 7 Poti h�fte rechts
	//this->HOTPOT = new float[8];
	this->HOTPOT.resize(8);
	 //Zeit in milisekunden
	this->time=0;
}


Sensor_Datenpacket::Sensor_Datenpacket(unsigned long zeit)
{
		 //Anzahl Floats welche zu einem Bytearray gepackt werden und wieder zur�ck converteriet werden
	this->numbersOfFloats = 100;

	 //Erste Nummer ist die Auswahl der IMU
	 //0 Fuss rechts, 1 Unterschenkel rechts, 2 Oberschenkel rechts, 3 Fuss links, 4 Unterschenkel links, 5 Oberschenkel links, 6 R�cken
	 //Zweite Auswhal ist die Auswahl des Werts
	 //0 Gyro X, 1 Gyro Y, 2 Gyro Z, 3 Acc X, 4 Acc Y, 5 Acc Z
	//this->IMU = new float[7][ 6];

	this->IMU.resize(7);
	for(int i =0;i<7;++i)
	{
		this->IMU[i].resize(6);
	}

	 //Erste Nummer ist auswahl rechts (0) links (1)
	 //Zweite Nummer ist auswahl der Postition
//	this->LOADCELL = new float[2][ 6];
	this->LOADCELL.resize(2);
	for(int i =0;i<2;++i)
	{
		this->LOADCELL[i].resize(6);
	}
	 //Erste Nummer ist auswahl motor
	 //0 Motor knie rechts, 1 Motor rolle rechts, 2 Motor h�fte rechts, 3 motor knie links, 4 motor rolle links, 5 motor h�fte links
	 //Zweite Nummer ist auswahl funktion
	 //0 RPM, 1 Temperatur, 2 Motorenstrom
	//this->MOTORENWERTE = new float[6][ 3];
	this->MOTORENWERTE.resize(6);
	for(int i =0;i<6;++i)
	{
		this->MOTORENWERTE[i].resize(3);
	}

	 //0 Hotpot knie link, 1 hotpot hebel links, 2 hotpot rolle link, 3 hotpot knie rechts, 4 hotpot hebel rechts, 5 hotpot rolle rechts, 6 Poti h�fte links, 7 Poti h�fte rechts
	//this->HOTPOT = new float[8];
	this->HOTPOT.resize(8);
	//Zeit in milisekunden
	this->time = zeit;
}

//Packt das Struct in ein Bytearray
std::vector<unsigned char> Sensor_Datenpacket::convertToByte()
{
	std::vector<unsigned char> packet (numbersOfFloats * 4);
	std::vector<float> reihe (numbersOfFloats);
	//std::vector<unsigned char>zwi (4);
	int position = 0;

	for (int i = 0; i < 7; i++)
	{
		for (int j = 0; j < 6; j++)
		{
			reihe[position] = IMU[i][j];
			position++;
		}
	}


	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 6; j++)
		{
			reihe[position] = LOADCELL[i][ j];
			position++;
		}
	}

	for (int i = 0; i < 6; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			reihe[position] = MOTORENWERTE[i][ j];
			position++;
		}
	}

	for (int i = 0; i < 8; i++)
	{
		reihe[position] = HOTPOT[i];
		position++;
	}



	for (int i = 0; i < numbersOfFloats; i++)
	{
		float f = reihe[i];
		char zwi[sizeof(float)];

		memcpy(zwi, &f, (size_t) sizeof(float));
		//zwi = BitConverter.GetBytes(reihe[i]);
		packet[4 * i] = zwi[0];
		packet[4 * i + 1] = zwi[1];
		packet[4 * i + 2] = zwi[2];
		packet[4 * i + 3] = zwi[3];
	}



	std::vector<unsigned char> msg = createHeader(packet,time,1);
	return msg;

}

//Entpackt ein Bytearray in die Struct daten
void Sensor_Datenpacket::convertFromByte(std::vector<unsigned char> msg)
{
	std::vector<unsigned char> packet = detachHeader(msg);
this->time = extractTime(msg);

	std::vector<float> reihe (numbersOfFloats);
	int position = 0;

	for (int i = 0; i < numbersOfFloats; i++)
	{
		//reihe[i] = BitConverter.ToSingle(packet, 4 * i);
		float f;
		unsigned char b[] = { packet[4 * i ], packet[4 * i + 1], packet[4 * i + 2], packet[4 * i+3] };
		memcpy(&f, &b, sizeof(f));

		reihe[i] = f;
	}

	

	for (int i = 0; i < 7; i++)
	{
		for (int j = 0; j < 6; j++)
		{
			IMU[i][ j] = reihe[position];
			position++;
		}
	}


	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 6; j++)
		{
			LOADCELL[i][ j] = reihe[position];
			position++;
		}
	}

	for (int i = 0; i < 6; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			MOTORENWERTE[i][ j] = reihe[position];
			position++;
		}
	}

	for (int i = 0; i < 8; i++)
	{
		HOTPOT[i] = reihe[position];
		position++;
	}

}
 int Sensor_Datenpacket::getAnzahlBytes()
{
	return (numbersOfFloats)* 4;
}
unsigned char Sensor_Datenpacket::getID()
{

	return 1;
}
