#ifndef Output_datenpacket_HEADER
#define Output_datenpacket_HEADER


#include <vector>
#include "IDatenPacket.h"


class Output_Datenpacket : IDatenPacket
{
public:


	Output_Datenpacket();

	Output_Datenpacket(unsigned long zeit);
	//Anzahl bytes in einem Output struct
	int anzahl_bytes;

	//16 Leds die �ber je ein bit gesteuert werden
	std::vector<unsigned char> led;

	//Value of the vibration motors byte 0 is right byte 1 is left
	std::vector<unsigned char> vib_motor;

	//On board sound tweeter
	unsigned char tweeter;

	//Zeit in milisekunden
	unsigned long time;

	


	void setTime(long ti);

	void setLED(int number, bool state);

	bool getLEDstate(int number);



	unsigned char getID();

	unsigned long getTime();

	std::vector<unsigned char> convertToByte();

	void convertFromByte(std::vector<unsigned char>  msg);





};

#endif
