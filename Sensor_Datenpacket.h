	

#ifndef SENS_HEADER
#define SENS_HEADER

#include "IDatenPacket.h"

#include <vector>



class Sensor_Datenpacket : IDatenPacket
{
public:
	//Anzahl Floats welche zu einem Bytearray gepackt werden und wieder zur�ck converteriet werden
	int numbersOfFloats;

	//Erste Nummer ist die Auswahl der IMU
	//0 Fuss rechts, 1 Unterschenkel rechts, 2 Oberschenkel rechts, 3 Fuss links, 4 Unterschenkel links, 5 Oberschenkel links, 6 R�cken
	//Zweite Auswhal ist die Auswahl des Werts
	//0 Gyro X, 1 Gyro Y, 2 Gyro Z, 3 Acc X, 4 Acc Y, 5 Acc Z
	vector<vector<float>> IMU;


	//Erste Nummer ist auswahl rechts (0) links (1)
	//Zweite Nummer ist auswahl der Postition
	vector<vector<float>> LOADCELL;

	//Erste Nummer ist auswahl motor
	//0 Motor knie rechts, 1 Motor rolle rechts, 2 Motor h�fte rechts, 3 motor knie links, 4 motor rolle links, 5 motor h�fte links
	//Zweite Nummer ist auswahl funktion
	//0 RPM, 1 Temperatur, 2 Motorenstrom
	vector<vector<float>> MOTORENWERTE;

	//0 Hotpot knie link, 1 hotpot hebel links, 2 hotpot rolle link, 3 hotpot knie rechts, 4 hotpot hebel rechts, 5 hotpot rolle rechts, 6 Poti h�fte links, 7 Poti h�fte rechts
 	vector<float>  HOTPOT;

	//Zeit in milisekunden
	unsigned long time;

	unsigned long get_time();
	
	Sensor_Datenpacket();
	
	Sensor_Datenpacket(unsigned long zeit);

	void setTime(long ti);

	//Packt das Struct in ein Bytearray
	std::vector<unsigned char> convertToByte();
	

	//Entpackt ein Bytearray in die Struct daten
	void convertFromByte(std::vector<unsigned char> msg);
	
	int getAnzahlBytes();
	
	unsigned char getID();



};

#endif
